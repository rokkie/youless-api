<?php

// add 'lib' to the include path
set_include_path(implode(PATH_SEPARATOR, [
  realpath(dirname(__FILE__) . '/../lib'),
  get_include_path(),
]));

// register an autoload function
spl_autoload_register(function ($class) {
  include_once $class . '.php';
});

// make sure we operate from a know working directory
chdir(dirname(__FILE__) . '/../');

// set default timezone to european time
date_default_timezone_set('Europe/Amsterdam');

// ready to rumble..
