<?php
// determine application root path
$root = dirname(__FILE__) . '/../';

// run the bootstrap
require_once $root . '/src/bootstrap.php';

// load the configuration
$config = parse_ini_file($root . '/etc/config.ini', true);

// instantiate objects
$db      = new Db($config['mysql']);
$youless = new Youless($config['youless']['host']);
$import  = new Import($db, $youless);

// read command line arguments
$types = array_slice($argv, 1);

// run the import for each type
array_map([$import, 'import'], $types);
