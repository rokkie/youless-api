<?php
// determine application root path
$root = dirname(__FILE__) . '/../';

// run the bootstrap
require_once $root . '/src/bootstrap.php';

// load the configuration
$config = parse_ini_file($root . '/etc/config.ini', true);

// instantiate objects
$db = new Db($config['mysql']);

// get query parameters from the request
$type        = RequestHelper::getQueryValue('type');
$dateStart   = RequestHelper::getQueryValue('dateStart');
$dateEnd     = RequestHelper::getQueryValue('dateEnd');
$granularity = RequestHelper::getQueryValue('granularity', 'min');

// start without errors
$errors = [];

// check if a correct type was given
if (!in_array($type, ['electricity', 'gas'])) {
  $errors['type'] = sprintf("Invalid type '%s' given. Must be one of electricity|gas.", $type);
}

// check if a correct granularity was given
if (!in_array($granularity, ['min', 'hour'])) {
  $errors['granularity'] = sprintf("Invalid grain '%s' given. Must be one of min|hour.", $granularity);
}

// check if start date is correct when given
if ($dateStart && false === preg_match('/\d+/', $dateStart)) {
  $errors['dateStart'] = sprintf("Invalid start date '%s' given. Must be numeric.", $dateStart);
}

// check if end date is correct when given
if ($dateEnd && false === preg_match('/\d+/', $dateEnd)) {
  $errors['dateEnd'] = sprintf("Invalid end date '%s' given. Must be numeric.", $dateEnd);
}

// check if start date is less than end date when both are given
if ($dateStart && $dateEnd && $dateStart >= $dateEnd) {
  $errors['date'] = 'Start date cannot be smaller than end date.';
}

// check if there were any errors
if (0 < count($errors)) {
  RequestHelper::sendJson($errors, 400);
}

// convert dates from unix time to datetime objects
$dateStart = $dateStart ? new DateTime('@' . $dateStart) : null;
$dateEnd   = $dateEnd   ? new DateTime('@' . $dateEnd) : null;

// fetch the usage based on the given parameters
$data = $db->fetchUsage($type, $granularity, $dateStart, $dateEnd);

// send date back the the client as JSON
RequestHelper::sendJson($data);
